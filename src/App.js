import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import FarmerPublicData from "./pages/FarmerPublicData";

function App() {
  return (
    <Router>
      <Switch>
        <Route component={FarmerPublicData} path="/farmer" exact />
      </Switch>
    </Router>
  );
}

export default App;
