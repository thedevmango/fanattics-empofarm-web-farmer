import React, { useEffect, useState } from "react";
import "./FarmerPublicData.css";
import { CheckmarkFilled16 } from "@carbon/icons-react";
import EmpofarmLogo from "./empofarm.png";
import qs from "query-string";
import { useHistory } from "react-router-dom";
import Axios from "axios";

// eslint-disable-next-line
export default () => {
  const history = useHistory();

  const [product, setProduct] = useState({});
  const [farmer, setFarmer] = useState({});

  const fetchProductData = async () => {
    const searchUrl = qs.parse(history.location.search);
    try {
      const { data } = await Axios.get(
        `https://empofarm-api.fanattics.tech/products/skus`,
        {
          params: {
            id: searchUrl.product,
            include: "farmer",
          },
        }
      );

      fetchFarmerData(data?.result[0]?.farmer?.userId);
      setProduct(data?.result[0]);
      // console.log(data.result[0]);
    } catch (err) {
      console.log(err);
      alert("Terjadi kesalahan di server");
    }
  };

  const fetchFarmerData = async (farmerUserId) => {
    try {
      const { data } = await Axios.get(
        `https://empofarm-api.fanattics.tech/users/farmers`,
        {
          params: {
            id: farmerUserId,
          },
        }
      );

      setFarmer(data.result[0]);
    } catch (err) {
      console.log(err);
      alert("Terjadi kesalahan di server");
    }
  };

  useEffect(() => {
    fetchProductData();
  }, []);
  return (
    <>
      <div className="brand-header">
        <img src={EmpofarmLogo} alt="empofarm" />
      </div>
      <div className="container py-4">
        <div className="card p-3 d-flex flex-row mt-3">
          <img
            className="farmer-profile"
            src={farmer?.profilePicture || ""}
            alt="farmer"
          />
          <div className="ml-2 d-flex flex-column">
            <div
              className="d-flex flex-row align-items-center"
              style={{ lineHeight: "18px" }}
            >
              <span className="subheading">{farmer?.fullName}</span>
              <CheckmarkFilled16 color="#69984C" className="ml-2" />
            </div>
            <span className="farmer-badge align-self-start mt-1">Farmer</span>
            <span className="caption mt-1">
              {farmer?.farmer?.farmName} - {farmer?.farmer?.address},{" "}
              {farmer?.farmer?.city}
            </span>
          </div>
        </div>
        <div className="card p-3 mt-3">
          <img
            className="farmer-farm"
            src={farmer?.farmer?.farmPhoto[0] || ""}
            alt="farm"
          />
          <span className="post-caption mt-3">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi
            amet iure itaque, adipisci labore, culpa quasi temporibus ipsum
            atque velit, quo repellendus neque. Rem iure esse suscipit
            voluptatum, qui praesentium?
          </span>
        </div>
        <div className="card p-3 mt-3 product-card d-flex flex-row">
          <img src={product?.product_variant?.imageUrl || ""} alt="product" />
          <div className="d-flex flex-column justify-content-center ml-2">
            <div className="subheading">
              {product?.product_variant?.productName}
            </div>
            <div
              className="subheading font-weight-normal"
              style={{ color: "#6e6e6e" }}
            >
              SKU: {product.sku}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
